package com.db.auth.mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtauthenticationwithmysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtauthenticationwithmysqlApplication.class, args);
	}

}
